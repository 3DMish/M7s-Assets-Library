# M7s-Assets-Library

All models in this repository, created by: [3DMish (Mish7913)](https://projects.blender.org/3DMish)

## Get Index JSON
### Python

``` pythonn
import requests;

rqs = requests.get('https://projects.blender.org/3DMish/M7s-Assets-Library/raw/branch/main/INDEX.json').json();

print rqs;
```

## Licenses

- CC-Zero (CC0)
- CC Attribution (CC-BY)
